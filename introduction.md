# Introduction {.unnumbered}

> « Habitant sed taciti leo curabitur torquent dignissim ipsum class molestie, cursus dapibus bibendum auctor pretium inceptos fermentum eleifend commodo, augue sagittis suspendisse facilisi fringilla finibus mus erat, scelerisque dui accumsan massa in vel phasellus arcu » [@auteur2022].

Voir @fig-exemple et @tbl-exemple ci-dessous.

![Logo de Quarto](img/quarto.png){#fig-exemple}

| Default | Left | Right | Center |
|---------|:-----|------:|:------:|
| 12      | 12   |    12 |   12   |
| 123     | 123  |   123 |  123   |
| 1       | 1    |     1 |   1    |

: Exemple de tableau {#tbl-exemple}