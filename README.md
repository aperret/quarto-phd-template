# Modèle Quarto pour thèse de doctorat

Ce dépôt contient un exemple d'utilisation de [Quarto](https://quarto.org) pour fabriquer une thèse de doctorat.

Si vous ne connaissez pas Quarto, il s'agit d'un outil qui permet de fabriquer des publications scientifiques complexes et multi-formats à partir de fichiers textes simples. Quarto est une sorte d'exosquelette pour [Pandoc](http://pandoc.org), un convertisseur de documents. Quarto réunit dans un système intégré et bien documenté toutes les possibilités offertes par Pandoc mais aussi par des utilitaires associés à Pandoc (comme l'excellent programme pandoc-crossref).

Le but de ce modèle n'est pas de recommander une manière de fabriquer sa thèse mais d'illustrer les possibilités offertes par Quarto pour intégrer dans un même système : la rédaction au format texte (en Pandoc Markdown) ; le processus de relecture et de corrections via un fichier pour traitement de texte ; l'export final multi-formats (PDF, HTML, EPUB…).

À partir d'un texte divisé en plusieurs fichiers (introduction, chapitre 1, chapitre 2…), Quarto est configuré pour exécuter deux processus :

- Générer une version « brouillon » : Quarto exporte individuellement chaque chapitre au format docx. On imagine ici une personne qui rédige et envoie ses chapitres au fur et à mesure à son directeur ou sa directrice de thèse, qui lui renvoie un fichier annoté.
- Générer la version finale : Quarto exporte l'ensemble de la thèse sous la forme d'un fichier PDF (nécessite l'installation d'une distribution LaTeX complète) mais aussi d'un site web, et pourquoi pas d'autres formats (EPUB par exemple).

Encore une fois, ceci ne constitue pas une recommandation à suivre mais un exemple qui montre comment on peut utiliser Quarto pour gérer toutes les étapes du processus de rédaction, relecture et publication. Le modèle peut être ajusté (ex : odt plutôt que docx) ou complètement transformé.

## Pré-requis

- [Installer Quarto](https://quarto.org/docs/get-started/).
- Pour l'export PDF : installer une distribution LaTeX. Ex : [TeXLive](https://tug.org/texlive/). D'expérience, je recommande plutôt une distribution complète qu'une distribution allégée. Si la seconde fait gagner du temps lors du téléchargement et de l'installation initiale, elle peut aussi en faire perdre dès qu'on commence à modifier son modèle LaTeX et qu'on doit aller chercher un à un les paquets manquants.

## Utilisation

Pour générer la version brouillon (dans un répertoire `_output-draft`) :

```
quarto render
```

Pour générer la version finale (dans un répertoire `_output`) :

```
quarto render --profile book
```

Pour prévisualiser la version finale :

```
quarto preview --profile book
```

Remarque : l'ordre d'écriture entre la commande `render` ou `preview` et l'option `--profile` n'a pas d'importance. On peut donc aussi faire :

```
quarto --profile book render
quarto --profile book preview
```

## Personnalisation

Pour modifier le modèle docx, il faut modifier les styles du document `template.docx`. Le plus simple est de générer l'export docx avec `quarto render`, puis ouvrir l'un des exports docx, modifier les styles jusqu'à obtenir le résultat voulu, enregistrer le fichier sous le nom `template.docx` et le mettre à la place du fichier du même nom à la racine du projet.

## Liens utiles

- [Documentation de Quarto](https://quarto.org/docs/guide/)
- [Générateur de tableaux en Markdown](https://www.tablesgenerator.com/markdown_tables)

